#include <stdint.h>
#include <stdlib.h>
#include <string.h>
//#include "uart.h"
#include "wazombi_boards.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"
#include "dbg.h"
#include "inc/SIM5360.h"
#include "inc/BMC156.h"
#include "twi_master.h"
#include "app_timer.h"
#include "softdevice_handler.h"


#define SIM_RX 25
#define SIM_TX 26

#define ACC_INT 16

#define APP_GPIOTE_MAX_USERS        2

/**< Value of the RTC1 PRESCALER register. */
#define APP_TMR_PRESCL				0
/**< Maximum number of simultaneously created timers. */
#define APP_TIMER_MAX_TIMERS		10
/**< Size of timer operation queues. */
#define APP_TIMER_OP_QUEUE_SIZE		7

#define SCHED_MAX_EVENT_DATA_SIZE   sizeof(app_timer_event_t)
/**< Maximum number of events in the scheduler queue. */
#define SCHED_QUEUE_SIZE            10

app_gpiote_user_id_t                h_gpiote_user                   = 0;
extern uint8_t setupForSmS();

void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name)
{
	LOG_INFO("Error Handler");
}

void interrupt_event_handler(uint32_t low_to_high, uint32_t high_to_low)
{
	if(low_to_high)
	{
		LOG_INFO("INTERRUPT!!!");
		sendSMS("37253340138", "Movement", false, NULL);
	}
}

void gpio_interrupt_enable(void){
	APP_GPIOTE_INIT(APP_GPIOTE_MAX_USERS);
	uint32_t pinmask    =  (1 << ACC_INT)/* | (1 << 13) |(1 << 18) |(1 << 19)*/;
	uint32_t err_code   = NRF_SUCCESS;

	nrf_gpio_cfg_input( ACC_INT,   NRF_GPIO_PIN_NOPULL);
//	nrf_gpio_cfg_input( 13,   NRF_GPIO_PIN_NOPULL);
//	nrf_gpio_cfg_input( 18,   NRF_GPIO_PIN_NOPULL);
//	nrf_gpio_cfg_input( 19,   NRF_GPIO_PIN_NOPULL);
	app_gpiote_user_enable(h_gpiote_user);

	err_code = app_gpiote_user_register(&h_gpiote_user,
			pinmask,
			pinmask,
			interrupt_event_handler);
	//LOG_DEBUG("Error: %x: %x, %x, %x", err_code, NRF_ERROR_INVALID_STATE, NRF_ERROR_INVALID_PARAM, NRF_ERROR_NO_MEM);
	CHECK_WARN_RE(err_code,
			NRF_SUCCESS,
			"Failed to inform the GPIOTE module that the specified user"
			"wants to use the GPIOTE module!");

	err_code = app_gpiote_user_enable(h_gpiote_user);
	//LOG_DEBUG("Error: %x: %x, %x, %x", err_code, NRF_ERROR_INVALID_STATE, NRF_ERROR_INVALID_PARAM, NRF_ERROR_NO_MEM);
	CHECK_WARN_RE(err_code,
			NRF_SUCCESS,
			"Failed to inform the GPIOTE module that the specified user"
			"is done using the GPIOTE module!");
}

int main(void)
{
	dbg_log_init();
	LOG_INFO("Hello World");

	uint8_t array[2];
	array[0] = 0x00;


	twi_master_init();

	//BMC156_ACC_reg_read(0x00, array, 1);

	//LOG_INFO("Got: %02X", array[0]);

	BMC156_ACC_init();
	gpio_interrupt_enable();
	twi_master_deinit();

	sim_uart_cfg(SIM_TX,SIM_RX);
	init_sim5360(1234);
	setupForSmS();
	//char text[256];

	//LOG_INFO("%s", text);

	while(1);
}
