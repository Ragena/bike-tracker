#include "inc/SIM5360.h"
#include "nrf_gpio.h"
#include "wazombi_boards.h"
#include "dbg.h"
#include "nrf.h"
#include "softdevice_handler.h"
#include "uart.h"

char sim_rx_buffer[SIM_BUFF_MAX];

char* sim_uart_buf_head = sim_rx_buffer;

uint8_t *sim_ACK, *sim_CHK;

bool issim_ACK;
bool isConn = false;

bool responseExpected;
bool smsReady = false;

bool sendCommand(const char* cmd, const char* ack , bool n_nl, uint8_t timeout);
bool waitonResponse(const char* ack, uint16_t timeout);

void uart_cb_handler ( void )
{
	if(responseExpected)
	{
		while ( NRF_UART0->EVENTS_RXDRDY != 0 )
		{
			*sim_uart_buf_head = uart_get();
			if(*sim_uart_buf_head == *sim_CHK && !issim_ACK)
			{
				sim_CHK++;
				if(sim_CHK >= sim_ACK+strlen(sim_ACK))
				{
					issim_ACK = true;
				}
			}
			else
			{
				sim_CHK = sim_ACK;
			}

			sim_uart_buf_head++;
		}
	}
	//LOG_DEBUG("%s", sim_rx_buffer);
}

void sim_uart_cfg(uint8_t tx, uint8_t rx)
{
	uart_config(0, tx, 0, rx, false, UART_BAUDRATE_BAUDRATE_Baud115200);
	uart_override_irqhandler(&uart_cb_handler);
}


uint8_t setupForSmS()
{
	LOG_INFO("Here");
	sendCommand("AT+CMGF=1", "OK", false, 5);
	sendCommand("AT+CNMI=2,1", "OK", false, 5);
	sendCommand("AT+CPMS=\"SM\",\"SM\",\"SM\"", "OK", false, 5);
	smsReady = true;
}

uint8_t init_sim5360(uint16_t pin)
{
	LOG_INFO("Initializing SIM5360");
	//TODO: Toggle Power pin.

	// Simple AT. Check if we even receive OK
	sendCommand("AT", "OK", false, 5);


	// Check if PIN is needed.
	sendCommand("AT+CPIN?", "+CPIN: READY", 0, 5);


	// Set the PIN, using the given pin parameter.
	uint8_t tx_buff[16];
	sprintf(tx_buff, "AT+CPIN=%d", pin);
	sendCommand(tx_buff, "+CPIN: READY", false, 5);



	// check if the Network Interface is already open.
	sendCommand("AT+NETOPEN?", "+NETOPEN: 0", false, 5);

	sendCommand("AT+NETOPEN", "+NETOPEN: 0", false, 15);

	return 0;
}

uint8_t connect(char* host, uint16_t port)
{
	LOG_INFO("Connecting to host %s:%u", host, port);

	if(!sendCommand("AT+NETOPEN?", "+NETOPEN: 1,0", false, 15)) // Check if the Network Interface is open. If not, there is no point in continuing.
		return 1;

	//This part is tricky. Because there is no AT command to check on the status of this, it must be very precise.
	/* How we do this:
	 * Try Starting HTTPS protocol stack. If it works, then cool. If not, then...
	 * Try stopping the stack (I assume that it is open). Then start it again.
	 * If neither of these works, then we have a problem.
	 */
	if(!sendCommand("AT+CHTTPSSTART", "OK", false, 5))
	{
		if(sendCommand("AT+CHTTPSSTOP", "OK", false, 5))
		{
			if(!sendCommand("AT+CHTTPSSTART", "OK", false, 5))
			{
				LOG_ERROR("Problems with HTTPS stack Start.");
				return 2;
			}
		}
		else
		{
			LOG_ERROR("BIG problems with HTTPS Stack.");
			return 3;
		}
	}

	// Opening connection to the given host through the given port.
	uint8_t tx_buff[strlen(host) + 22];
	sprintf(tx_buff, "AT+CHTTPSOPSE=\"%s\",%u,1", host, port); // Check AT command set for HTTP/HTTPS setting.
	if(sendCommand(tx_buff, "OK", false, 15))
	{
		isConn = true;
		return 0;
	}

	LOG_ERROR("Unable to connect to host %s:%u", host, port);
	return 4;
}

bool is_connected()
{
	return isConn;
}

bool disconnect()
{
	isConn = false;
	return sendCommand("AT+CHTTPSCLSE", "OK", false, 15);
}

uint8_t getIPaddress(char* IP_buf)
{
	if(!sendCommand("AT+NETOPEN?", "+NETOPEN: 1,0", false, 15)) // Check if the Network Interface is open. If not, there is no point in continuing.
		return 1;

	if(sendCommand("AT+IPADDR", "OK", false, 15))
	{
		char* ptr = strstr(sim_rx_buffer, "ADDR:") + 6; // Trying to find the IP address in the sim_rx_buffer
		char* chk;
		if(!(ptr-6))
		{
			LOG_ERROR("Can't find IP despite OK in string: %s", sim_rx_buffer);
			return 2;
		}

		// Since the end of the IP address isn't the end of the sim_rx_buffer string, we must find the actual end of the IP and force it to be the end of the sim_rx_buffer string
		for(chk = ptr + 5; chk < ptr+strlen(ptr); chk++)
		{
			if(!(*chk >= '0' && *chk <= '9' || *chk == '.'))
			{
				*chk = 0;
				break;
			}
		}

		// Copy the IP address to the given char array. Here is where the forced end of string comes into play, as we don't know how long the IP address may be.
		strcpy(IP_buf, ptr);
	}

	else
	{
		LOG_ERROR("AT+IPADDR didn't end in OK, but net is open. Msg: %s", sim_rx_buffer);
	}

	return 0;
}

bool reset()
{
	if(!sendCommand("AT+REBOOT", "OK", false, 10))
	{
		LOG_ERROR("Reboot failed.");
		return false;
	}
	smsReady = false;
	isConn = false;
	return true;
}

uint8_t reboot(uint16_t pin)
{
	if(reset())
		return init_sim5360(pin);
	return 0x10;
}

uint8_t httpQuerry(char* out, char* in, uint8_t timeout)
{
	if(!out || !(*out))
	{
		LOG_ERROR("Querry seems to be NULL");
		return 1;
	}

	if(!timeout)
	{
		LOG_DEBUG("Timeout was 0. Using default of 30");
	}

	char cmd [32];

	sprintf(cmd, "AT+CHTTPSSEND=%d", strlen(out));
	if(sendCommand(cmd, ">", false, 5))
	{
		if(sendCommand(out, "OK", false, timeout))
		{
			//if(waitonResponse("RECV EVENT", timeout))
			//{
				if(!sendCommand("AT+CHTTPSRECV=256", "OK", false, timeout))
				{
					LOG_ERROR("No RECV: %s", sim_rx_buffer);
					return 4;
				}
				LOG_INFO("OK! %s", sim_rx_buffer);
				return 0;
			//}
			//LOG_ERROR("No RECV: %s", sim_rx_buffer);
			//return 5;
		}
		else
		{
			LOG_ERROR("ERMMM.... Send brokesed");
			return 2;
		}
	}
	else
	{
		LOG_ERROR("ERMMM.... Send brokedes");
		return 3;
	}

}

uint8_t sendSMS(char* number, char* text, bool response, char* ack)
{
	size_t len = strlen(text);

	if(smsReady)
	{
		char cmd[(len + 5>32) ? (len + 5) : (32)];
		sprintf(cmd, "AT+CMGS=\"%s\"\r", number);
		sendCommand(cmd, ">", true, 5);
		sprintf(cmd, "%s%c", text, 0x1A);
		if(sendCommand(cmd, "ERROR", false, 5))
		{
			LOG_ERROR("Error in sending text to SIM module. %s", sim_rx_buffer);
			return 4;
		}
	}
	if(response)
	{
		return waitOnSMS(300, ack); // Consider allowing for conf.
	}
	return 0;
}

uint8_t waitOnSMS(uint16_t timeout, char* SMS)
{
	//char addr;
	if(waitonResponse("+CMTI: \"SM\"", timeout))
	{
		char *ptr = strstr(sim_rx_buffer, "CMTI:") + 11;
		if(!(ptr-11))
		{
			LOG_ERROR("Can't find SMS thingy despite sim_ACK coming.");
			return 1;
		}
		char cmd[10];
		sprintf(cmd, "AT+CMGR=%c\r", *ptr);
		if(sendCommand(cmd, "OK", false, 5))
		{
			strcpy(SMS, sim_rx_buffer);
			return 0;
		}
		else
		{
			LOG_ERROR("Problem receiving SMS from SIM");
			return 2;
		}
	}
	LOG_ERROR("Timeout on waiting for reply");
	return 3;
}

bool waitonResponse(const char* ack, uint16_t timeout)
{
	//clear the buffer.
	memset(sim_rx_buffer, 0, 256);
	sim_ACK = ack;
	sim_CHK = sim_ACK;
	sim_uart_buf_head = sim_rx_buffer;
	issim_ACK = false;
	responseExpected = true;

	uint16_t i;
	for(i = 0; i < timeout; i++)
	{
		nrf_delay_ms(1000);
		LOG_INFO("Waiting: %d, Ack: %x", i, issim_ACK);
		if(issim_ACK)
			break;
	}
	responseExpected = false;
	LOG_DEBUG("%d", issim_ACK);

	//TODO: Flush RX.
	return issim_ACK;
}

bool sendCommand(const char* cmd, const char* ack, bool n_nl, uint8_t timeout)
{
	//clear the buffer.
	memset(sim_rx_buffer, 0, 256);
	sim_ACK = ack;
	sim_CHK = sim_ACK;
	sim_uart_buf_head = sim_rx_buffer;
	issim_ACK = false;

	LOG_DEBUG("%s", cmd);
	responseExpected = true;

	if(n_nl)
		uart_putstring(cmd);
	else
		uart_putstringln(cmd);
	uint8_t i;
	for(i = 0; i < timeout; i++)
	{
		nrf_delay_ms(1000);
		LOG_INFO("Waiting: %d, Ack: %x", i, issim_ACK);
		if(issim_ACK)
			break;
	}
	responseExpected = false;
	LOG_DEBUG("%d", issim_ACK);

	//TODO: Flush RX.
	return issim_ACK;
}
