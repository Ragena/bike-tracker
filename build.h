#ifndef BUILD_H__
#define BUILD_H__

#define MACHINE_ID_STR "andreas"

#define BUILD_TIMESTAMP "201602121140"

#define WAZOMBI_SDK_GIT_COMMIT "f361cf9"

#define PROJ_GIT_COMMIT "6151a92"

#define PROJ_GIT_LAST_TAG ""

#define NORDIC_SDK_VER "6.1"

#endif
