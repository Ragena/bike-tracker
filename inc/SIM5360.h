#ifndef SIM5360_H
#define SIM5360_H

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define DEFAULT_WAIT_RSIM_TIMEOUT 500
#define SIM_TCP_TYPE 1
#define SIM_UDP_TYPE 0
#define SIM_BUFF_MAX 256

//uint16_t sim_pin;

void sim_uart_cfg(uint8_t tx, uint8_t rx);

uint8_t init_sim5360(uint16_t pin);

uint8_t sim_connect(char* host, uint16_t port);

bool sim_is_connected();

bool sim_disconnect();

uint8_t sim_getIPaddress(char* IP_buf);

bool sim_reset();

uint8_t sim_reboot(uint16_t pin);

uint8_t sim_httpQuerry(char* out, char* in, uint8_t timeout);

uint8_t sendSMS(char* number, char* text, bool response, char* ack);

uint8_t waitOnSMS(uint16_t timeout, char* SMS);

uint8_t getGPSCoord(char* coords);
//bool sendCommand(const char* cmd, const char* ack , char* res, uint8_t timeout);

#endif
