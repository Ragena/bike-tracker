#ifndef BMC156_H__
#define BMC156_H__

#ifndef I2C_WRITE
#define I2C_WRITE				0x00
#endif
#ifndef I2C_READ
#define I2C_READ				0x01
#endif

#define BMC156_ACC_ADDRESS 		0x20
#define BMC156_MAG_ADDRESS 		0x24

//Accelerometer registers
#define BMC156_ACC_WHO_AM_I		0x00	// Device ID Number, 0xFA

#define BMC156_ACC_OUT_X_LSB	0x02	// Bits [7:4] are 4 lower bits of X measurement, Bit 0 is new data flag
#define BMC156_ACC_OUT_X_MSB	0x03	// Bits [11:4] of X measurement
#define BMC156_ACC_OUT_Y_LSB	0x04	// Bits [7:4] are 4 lower bits of Y measurement, Bit 0 is new data flag
#define BMC156_ACC_OUT_Y_MSB	0x05	// Bits [11:4] of Y measurement
#define BMC156_ACC_OUT_Z_LSB	0x06	// Bits [7:4] are 4 lower bits of Z measurement, Bit 0 is new data flag
#define BMC156_ACC_OUT_Z_MSB	0x07	// Bits [11:4] of Z measurement

#define BMC156_ACC_TEMP			0x08	// Current chip temperature, 0x00 = 23 degrees

#define BMC156_ACC_INT_STATUS_0	0x09	// Interrupt status flags -> flat_int|orient_int|s_tap_int|d_tap_int|slo_no_mot_int|slope_int|high_int|low_int
#define BMC156_ACC_INT_STATUS_1	0x0A	// Interrupt status flags -> data_int|fifo_wm_int|fifo_full_int
#define BMC156_ACC_INT_STATUS_2	0x0B	// Interrupt status flags -> tap_sign|tap_first_z|tap_first_y|tap_first_x|slope_sign|slope_first_z|slope_first_y|slope_first_x
#define BMC156_ACC_INT_STATUS_3	0x0C	// Interrupt status flags -> flat|orient:2|orient:1|orient:0|high_sign|high_first_z|high_first_y|high_first_x

#define BMC156_ACC_FIFO_STATUS	0x0E	// fifo_overrun|fifo_frame_counter[6:0]

#define BMC156_ACC_RANGE_SELECT	0x0F	// Bits [3:0] select accelerometer measurement range
#define BMC156_ACC_BANDW_SELECT	0x10	// Bits [4:0] select accelerometer data filter bandwidth
#define BMC156_ACC_PWR_MODE		0x11	// Power modes: suspend|low_power|deep_suspend|sleep_dur[4:1]
#define BMC156_ACC_LOW_PWR		0x12	// Bit 6 low power mode, bit 5 sleep timer mode
#define BMC156_ACC_DATA_ACQ		0x13	// Data acquisition and data output format
#define	BMC156_ACC_SOFTRESET	0x14	// User triggered reset. 0xB6 triggers a reset, everything else is ignored
#define BMC156_ACC_INT_EN0		0x16	// Interrupt engine 0 controls -> flat_en|orient_en|s_tap_en|d_tap_en|reserved|slope_en_z|slope_en_y|slope_en_x
#define BMC156_ACC_INT_EN1		0x17	// Interrupt engine 1 controls -> reserved|int_fwm_en|int_ffull_en|data_en|low_en|high_en_z|high_en_y|high_en_x
#define BMC156_ACC_INT_EN2		0x18	// Interrupt engine 2 controls -> reserved[7:4]|slo_no_mot_sel|slo_no_mot_en_z|slo_no_mot_en_y|slo_no_mot_en_x
#define	BMC156_ACC_INT_MAP1		0x1A	// INT2 pin interrupt mapping -> int2_data|int2_fwm|int2_ffull|reserved[4:0]
#define BMC156_ACC_INT_MAP2		0x1B	// INT2 pin interrupt mapping -> int2_flat|int2_orient|int2_s_tap|int2_d_tap|int2_slo_no_mot|int2_slope|int2_high|int2_low
#define BMC156_ACC_INT_SRC		0x1E	// interrupt data source definition -> reserved[7:6]|int_src_data|int_src_tap|int_src_slo_no_mot|int_src_slope|int_src_high|int_src_low
#define BMC156_ACC_INT_OUT_CTRL	0x20	// interrupt pin behaviour -> bit3: open-drain, bit2: active high level
#define	BMC156_ACC_INT_RST_LTCH	0x21	// Interrupt reset bit and the interrupt mode selection
#define BMC156_ACC_INT0			0x22	// Time definition for the low-g interrupt
#define BMC156_ACC_INT1			0x23	// Threshold definition for the low-g interrupt
#define BMC156_ACC_INT2			0x24	// Low-g interrupt mode selection, low-g hysteresis setting and high-g hysteresis
#define BMC156_ACC_INT3			0x25	// Time definition for the high-g interrupt
#define BMC156_ACC_INT4			0x26	// Threshold definition for the high-g interrupt
#define BMC156_ACC_INT5			0x27	// Slope interrupt sampling number and slow/no-motion interrupt trigger delay
#define BMC156_ACC_INT6			0x28	// Threshold definition for the any-motion interrupt
#define BMC156_ACC_INT7			0x29	// Threshold definition for the slow/no-motion interrupt
#define BMC156_ACC_INT8			0x2A	// Timing definitions for the single tap and double tap interrupts
#define BMC156_ACC_INT9			0x2B	// Single and double tap interrupt sampling number and threshold definition
#define BMC156_ACC_INTA			0x2C	// Hysteresis, blocking and mode for the orientation interrupt
#define BMC156_ACC_INTB			0x2D	// Axis orientation, up/down masking and theta blocking angle for the orientation interrupt
#define BMC156_ACC_INTC			0x2E	// Flat threshold angle definition for flat interrupt
#define BMC156_ACC_INTD			0x2F	// Definition of flat interrupt hold time and flat interrupt hysteresis
#define BMC156_ACC_SELFTEST		0x32	// Setting for the sensor self-test configuration and trigger
#define BMC156_ACC_NVM_CTRL		0x33	// Control settings for the few-time programmable non-volatile memory
#define BMC156_ACC_SPI3_WDT		0x34	// Settings for the digital interfaces
#define BMC156_ACC_OFC_CTRL		0x36	// Offset compensation control
#define BMC156_ACC_OFC_SETTING	0x37	// Offset compensation settings
#define BMC156_ACC_OFC_X		0x38	// Offset compensation value for x-axis acceleration data
#define BMC156_ACC_OFC_Y		0x39	// Offset compensation value for y-axis acceleration data
#define BMC156_ACC_OFC_Z		0x3A	// Offset compensation value for z-axis acceleration data
#define BMC156_ACC_NVM_GP0		0x3B	// General purpose data register with NVM back-up
#define BMC156_ACC_NVM_GP1		0x3C	// General purpose data register with NVM back-up
#define BMC156_ACC_FIFO_CFG0	0x30	// FIFO watermark level
#define BMC156_ACC_FIFO_CFG1	0x3E	// FIFO configuration settings
#define BMC156_ACC_FIFO_DATA	0x3F	// FIFO data readout register

//Magnetometer registers
#define BMC156_MAG_WHO_AM_I		0x40	// Device ID Number, 0x32

#define BMC156_MAG_OUT_X_LSB	0x42	// Bits [7:3] are 5 lower bits of X measurement, Bit 0 is self-test result flag
#define BMC156_MAG_OUT_X_MSB	0x43	// Bits [12:5] of X measurement
#define BMC156_MAG_OUT_Y_LSB	0x44	// Bits [7:3] are 5 lower bits of Y measurement, Bit 0 is self-test result flag
#define BMC156_MAG_OUT_Y_MSB	0x45	// Bits [12:5] of Y measurement
#define BMC156_MAG_OUT_Z_LSB	0x46	// Bits [7:1] are 7 lower bits of Z measurement, Bit 0 is self-test result flag
#define BMC156_MAG_OUT_Z_MSB	0x47	// Bits [14:7] of Z measurement
#define BMC156_MAG_RHALL_LSB	0x48	// Bits [7:2] are 6 lower bits of RHALL measurement, Bit 0 is data ready (DRDY) status bit
#define BMC156_MAG_RHALL_MSB	0x49	// Bits [13:6] of RHALL measurement

#define BMC156_MAG_INT_STATUS	0x4A	// Bit 7 is data overrun status flag, bit 6 is overflow status flag
#define BMC156_MAG_PWR_REG		0x4B	// Bits 7 and 1 are soft reset bits, bit 2 is SPI3_enable bit and bit 0 is power control enable bit
#define BMC156_MAG_OP_CTRL		0x4C	// Bits [7:6] advanced self-test control bits, bits [5:3] data rate control bits, bits [2:1] operation mode control bits, bit [0] normal self-test bit
#define BMC156_MAG_DATA_CTRL	0x4E	// Bit 7 is DRDY pin enable bit, bits [5:3] axis enable bits (ZYX, active low), bit 2 DRDY pin polarity (0=active low, 1=active high)
#define BMC156_MAG_XY_REP_CTRL	0x51	// X/Y-axis repetition control register
#define BMC156_MAG_Z_REP_CTRL	0x52	// Z-axis repetition control register


uint32_t BMC156_MAG_reg_write(uint8_t reg, uint8_t value);
uint32_t BMC156_MAG_reg_read(uint8_t reg, uint8_t *value, uint8_t length);
uint32_t BMC156_ACC_reg_write(uint8_t reg, uint8_t value);
uint32_t BMC156_ACC_reg_read(uint8_t reg, uint8_t *value, uint8_t length);

uint32_t BMC156_ACC_init(void);
uint32_t BMC156_MAG_init(void);

uint32_t BMC156_MAG_get_data(int16_t *x, int16_t *y, int16_t *z);
uint32_t BMC156_ACC_get_data(int16_t *x, int16_t *y, int16_t *z);

#endif  // BMC156_H__
