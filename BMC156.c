

#include "twi_master.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "dbg.h"
#include "inc/BMC156.h"

uint32_t BMC156_MAG_reg_write(uint8_t reg, uint8_t value)
{
        uint8_t array[2];
        array[0] = reg;
        array[1] = value;

    if (!twi_master_transfer(
                BMC156_MAG_ADDRESS | I2C_WRITE,
                array,
                2,
                TWI_ISSUE_STOP
            ))
        return NRF_ERROR_INVALID_DATA;
    return NRF_SUCCESS;
}

uint32_t BMC156_MAG_reg_read(uint8_t reg, uint8_t *value, uint8_t length)
{
    if (!twi_master_transfer(
    			BMC156_MAG_ADDRESS | I2C_WRITE,
                &reg,
                1,
                TWI_DONT_ISSUE_STOP
            ))
        return NRF_ERROR_INVALID_DATA;
    if (!twi_master_transfer(
    			BMC156_MAG_ADDRESS | I2C_READ,
                value,
                length,
                TWI_ISSUE_STOP
            ))
        return NRF_ERROR_INVALID_DATA;

    return NRF_SUCCESS;
}

uint32_t BMC156_ACC_reg_write(uint8_t reg, uint8_t value)
{
        uint8_t array[2];
        array[0] = reg;
        array[1] = value;

    if (!twi_master_transfer(
                BMC156_ACC_ADDRESS | I2C_WRITE,
                array,
                2,
                TWI_ISSUE_STOP
            ))
        return NRF_ERROR_INVALID_DATA;
    return NRF_SUCCESS;
}


uint32_t BMC156_ACC_reg_read(uint8_t reg, uint8_t *value, uint8_t length)
{
    if (!twi_master_transfer(
    			BMC156_ACC_ADDRESS | I2C_WRITE,
                &reg,
                1,
                TWI_DONT_ISSUE_STOP
            ))
        return NRF_ERROR_INVALID_DATA;
    if (!twi_master_transfer(
    			BMC156_ACC_ADDRESS | I2C_READ,
                value,
                length,
                TWI_ISSUE_STOP
            ))
        return NRF_ERROR_INVALID_DATA;

    return NRF_SUCCESS;
}

uint32_t BMC156_ACC_init(void)
{
	BMC156_ACC_reg_write(BMC156_ACC_PWR_MODE, 0x00); // switch from suspend to normal mode
	nrf_delay_ms(1);
	BMC156_ACC_reg_write(BMC156_ACC_BANDW_SELECT, 0x08); // accelerometer data filter bandwidth 7.81hz
	nrf_delay_ms(1);
	BMC156_ACC_reg_write(BMC156_ACC_INT_EN0, 0x07); // enable slope/any-motion interrupt
	nrf_delay_ms(1);
	BMC156_ACC_reg_write(BMC156_ACC_INT_MAP2, 0x04); // map slope/any-motion interrupt to INT2 pin
	nrf_delay_ms(1);
	BMC156_ACC_reg_write(BMC156_ACC_INT_SRC, 0x04); // unfiltered data for slope/any-motion interrupt
	nrf_delay_ms(1);
	BMC156_ACC_reg_write(0x21, 0x06); // Set interrupt to temporary latch, 8s.
	nrf_delay_ms(1);
	BMC156_ACC_reg_write(BMC156_ACC_INT5, 0x03); // If 4 consecutive data points are above threshhold, raise interrupt
	nrf_delay_ms(1);
	BMC156_ACC_reg_write(BMC156_ACC_INT6, 0x07); // Set any-motion threshold.
	nrf_delay_ms(1);

    return NRF_SUCCESS;
}

uint32_t BMC156_MAG_init(void)
{
	uint8_t tmp = 0;
	for(uint8_t i = 0;i<10;i++){
		BMC156_MAG_reg_write(BMC156_MAG_PWR_REG, 0x01); // switch from suspend to sleep mode
		nrf_delay_ms(1);
		BMC156_MAG_reg_read(BMC156_MAG_PWR_REG, &tmp, 1);
		nrf_delay_us(50);
		if(tmp == 0x01){
			LOG_DEBUG("Exit pwr loop, i: %d", i);
			break;
		}
	}
	tmp = 0;
	for(uint8_t i = 0;i<10;i++){
			BMC156_MAG_reg_write(BMC156_MAG_XY_REP_CTRL, 0xFF); // 511 reps on xy axis
			nrf_delay_ms(1);
			BMC156_MAG_reg_read(BMC156_MAG_XY_REP_CTRL, &tmp, 1);
			nrf_delay_us(50);
			if(tmp == 0xFF){
				LOG_DEBUG("Exit XY rep loop, i: %d", i);
				break;
			}
		}
	tmp = 0;
	for(uint8_t i = 0;i<10;i++){
			BMC156_MAG_reg_write(BMC156_MAG_Z_REP_CTRL, 0xFF); // 256 reps on z axis
			nrf_delay_ms(1);
			BMC156_MAG_reg_read(BMC156_MAG_Z_REP_CTRL, &tmp, 1);
			nrf_delay_us(50);
			if(tmp == 0xFF){
				LOG_DEBUG("Exit Z rep loop, i: %d", i);
				break;
			}
		}
	tmp = 0;
	for(uint8_t i = 0;i<10;i++){
			BMC156_MAG_reg_write(BMC156_MAG_OP_CTRL, 0x00); // normal mode 10hz
			nrf_delay_ms(1);
			BMC156_MAG_reg_read(BMC156_MAG_OP_CTRL, &tmp, 1);
			nrf_delay_us(50);
			if(tmp == 0x00){
				LOG_DEBUG("Exit OP ctrl loop, i: %d", i);
				break;
			}
		}
	tmp = 0;
	for(uint8_t i = 0;i<10;i++){
				BMC156_MAG_reg_write(BMC156_MAG_DATA_CTRL, 0x84); // enable DRDY
				nrf_delay_ms(1);
				BMC156_MAG_reg_read(BMC156_MAG_DATA_CTRL, &tmp, 1);
				nrf_delay_us(50);
				LOG_DEBUG("DATA_CTRL: %x", tmp);
				if((tmp & 0x84) == 0x84){
					LOG_DEBUG("Exit DATA ctrl loop, i: %d", i);
					break;
				}
			}

    return NRF_SUCCESS;
}

uint32_t BMC156_MAG_get_data(int16_t *x, int16_t *y, int16_t *z){

//	twi_master_init();
	uint32_t err_code;

	uint8_t array[6] = {0,0,0,0,0,0};

	err_code = BMC156_MAG_reg_read(BMC156_MAG_OUT_X_LSB, array, 6);

//	twi_master_deinit();
	*x = ((array[1] << 8) & 0xFF00);
	*x |= (array[0] & 0xF8);
	*x = (*x >> 3);
	if(*x >= 0x1000){
		*x -= 0x2000;
	}

	*y = ((array[3] << 8) & 0xFF00);
	*y |= (array[2] & 0xF8);
	*y = (*y >> 3);
	if(*y >= 0x1000){
		*y -= 0x2000;
	}

	*z = ((array[5] << 8) & 0xFF00);
	*z |= (array[4] & 0xFE);
	*z = (*z >> 1);
	if(*z >= 0x4000){
		*z -= 0x8000;
	}

	return err_code;
}

uint32_t BMC156_ACC_get_data(int16_t *x, int16_t *y, int16_t *z){

//	twi_master_init();
	uint32_t err_code;

	uint8_t array[6] = {0,0,0,0,0,0};

	err_code = BMC156_ACC_reg_read(BMC156_ACC_OUT_X_LSB, array, 6);

//	twi_master_deinit();
	*x = ((array[1] << 8) & 0xFF00);
	*x |= (array[0] & 0xF0);
	*x = (*x >> 4);
	if(*x >= 0x800){
		*x -= 0x1000;
	}

	*y = ((array[3] << 8) & 0xFF00);
	*y |= (array[2] & 0xF0);
	*y = (*y >> 4);
	if(*y >= 0x800){
		*y -= 0x1000;
	}

	*z = ((array[5] << 8) & 0xFF00);
	*z |= (array[4] & 0xF0);
	*z = (*z >> 4);
	if(*z >= 0x800){
		*z -= 0x1000;
	}

	return err_code;
}
